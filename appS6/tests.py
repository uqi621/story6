from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import home
from .models import Status
from .forms import Status_Form
from django.apps import apps
from .apps import Apps6Config
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story6Test(TestCase):
	def test_apps(self):
		self.assertEqual(Apps6Config.name, 'appS6')
		self.assertEqual(apps.get_app_config('appS6').name, 'appS6')
	def test_url(self):
		response = Client().get('/status')
		self.assertNotEqual(response.status_code, 200)
		response = Client().get('/home')
		self.assertNotEqual(response.status_code, 200)
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_function(self):
		foundFunc = resolve('/')
		self.assertEqual(foundFunc.func, home)
	def test_model(self):
		status = Status.objects.create(status='ABCDEFGH')
		self.assertIsInstance(status, Status)#test object creation
	def test_post_story6(self):
		response_post = Client().post('/', {'status':50*'uvuvwevwevwe onyeteyevwe ughwemubwem ossas'})
		self.assertEqual(response_post.status_code, 302)
		response = Client().get('/')

