from django.db import models
from django.utils import timezone
from datetime import datetime

class Status(models.Model):
	status = models.TextField(max_length=300)
	datetime = models.DateTimeField(default=datetime.now,blank=True)