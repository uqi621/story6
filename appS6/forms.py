from django import forms
from .models import Status

class Status_Form(forms.ModelForm):

	status = forms.CharField(widget=forms.Textarea(attrs={
                "class" : "messagefields full",
                "required" : True,
                "placeholder":"Status",
                }))     

	class Meta:
		model = Status
		fields = ['status']