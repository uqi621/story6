from django.urls import include, path
from . import views

app_name = 'appS6'

urlpatterns = [
    path('', views.home, name = 'home'),
]