from django.shortcuts import render, redirect
from .models import Status
from .forms import Status_Form
from django.utils import timezone
import datetime

# Create your views here.
def home(request):
	form = Status_Form(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			form.save()
			return redirect('home')
	story = Status.objects.order_by("datetime").reverse()
		

	return render (request, 'home.html', {'form': form, 'story':story})
