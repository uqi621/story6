# Generated by Django 2.2.6 on 2019-11-05 12:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appS6', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.TextField(max_length=300),
        ),
    ]
